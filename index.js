const express = require('express');
const requestify = require('requestify');

var app = express();

app.listen(3000);

app.get('/test', function (request, response, next) {
	console.log('first request sent');
	requestify.get('http://169.254.169.254/latest/meta-data/public-ipv4')
		.then(function (response2) {
			console.log('second request sent');
			response.send(response2);
		});
});

app.get("/healthcheck", (request, response) => {
    response.send('healthy');
});
